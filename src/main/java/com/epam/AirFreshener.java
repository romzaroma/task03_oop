package com.epam;

public class AirFreshener extends HouseholdChemicals {
    private String aroma;
    private String type_of_application;

    public AirFreshener(String brend, String destination, String volume, int price, String availability, String aroma, String type_of_application) {
        super(brend, destination, volume, price, availability);
        this.aroma = aroma;
        this.type_of_application = type_of_application;
    }

    @Override
    public String toString() {
        return "AirFreshener{" + super.toString()+
                "aroma='" + aroma + '\'' +
                ", type_of_application='" + type_of_application + '\'' +
                '}';
    }
}
