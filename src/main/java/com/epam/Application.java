package com.epam;

public class Application {
    public static void main(String[] args) {
         HouseholdChemicals AirFreshener_1 =new AirFreshener("Glade","refreshing", "1 litr",75,"in stock","juicy berries","aerosol");
        HouseholdChemicals AirFreshener_2 =new AirFreshener("AirWick","refreshing","3 piece",110,"in stock","air harmony","aromablock");
        HouseholdChemicals AirFreshener_3 =new AirFreshener("Domo","refreshing","3 piece",64,"in the way","sunny citrus","aromablock");
        HouseholdChemicals AirFreshener_4 =new AirFreshener("Pure","refreshing","1 litr",200,"in stock","japanese garden","auto aerosol");
        System.out.println(AirFreshener_1);
        System.out.println(AirFreshener_2);
        System.out.println(AirFreshener_3);
        System.out.println(AirFreshener_4);
        System.out.println();
        HouseholdChemicals BleachesStainRemoval_1 =new BleachesStainRemoval("Vanish","bleach","750 g",80,"in stock", "powder","for all types of fabrics");
        HouseholdChemicals BleachesStainRemoval_2 =new BleachesStainRemoval("SANO","bleach","1,5 l",69,"in stock","liquid","for wool and silk");
        HouseholdChemicals BleachesStainRemoval_3 =new BleachesStainRemoval("DOS","bleach","60 g",16,"in the way","powder","for silk");
        HouseholdChemicals BleachesStainRemoval_4 =new BleachesStainRemoval("ACE","bleach","0,45 l",37,"in the way","liquid","for all types of fabrics exept wool and silk");
        System.out.println(BleachesStainRemoval_1);
        System.out.println(BleachesStainRemoval_2);
        System.out.println(BleachesStainRemoval_3);
        System.out.println(BleachesStainRemoval_4);
        System.out.println();
        HouseholdChemicals CleaningProducts_1 = new CleaningProducts("Barbuda","cleaning","0,75 l",27,"in stock","for the floor","wooden");
        HouseholdChemicals CleaningProducts_2 = new CleaningProducts("Emsal","cleaning","1,2 l",110,"in the way","for the floor","tile and laminate");
        HouseholdChemicals CleaningProducts_3 = new CleaningProducts("Mr.Proper","cleaning","1 l",164,"in stock","for the floor","wooden");
        HouseholdChemicals CleaningProducts_4 = new CleaningProducts("Mr.Proper","cleaning","0,75 l",114,"in stock","for furniture","wooden");
        HouseholdChemicals CleaningProducts_5 = new CleaningProducts("Pronto","cleaning","0,3 l",19,"in stock","for furniture","wooden");
        HouseholdChemicals CleaningProducts_6 = new CleaningProducts("Glutoclean","cleaning","1 l",64,"in the way","for furniture","wooden");
        HouseholdChemicals CleaningProducts_7 = new CleaningProducts("TUBA","cleaning","0,5 l",38,"in the way","for upholstered furniture","tissue");
        System.out.println(CleaningProducts_1);
        System.out.println(CleaningProducts_2);
        System.out.println(CleaningProducts_3);
        System.out.println(CleaningProducts_4);
        System.out.println(CleaningProducts_5);
        System.out.println(CleaningProducts_6);
        System.out.println(CleaningProducts_7);
    }
}
