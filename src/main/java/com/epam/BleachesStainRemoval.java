package com.epam;

public class BleachesStainRemoval extends HouseholdChemicals{
    private String kind_of_remedy;
    private String type_of_fabric;

    public BleachesStainRemoval(String brend, String destination, String volume, int price, String availability, String kind_of_remedy, String type_of_fabric) {
        super(brend, destination, volume, price, availability);
        this.kind_of_remedy = kind_of_remedy;
        this.type_of_fabric = type_of_fabric;
    }

    @Override
    public String toString() {
        return "BleachesStainRemoval{" + super.toString()+
                "kind_of_remedy='" + kind_of_remedy + '\'' +
                ", type_of_fabric='" + type_of_fabric + '\'' +
                '}';
    }
}
