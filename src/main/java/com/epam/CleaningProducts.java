package com.epam;

public class CleaningProducts extends HouseholdChemicals{
    private String place_of_application;
    private String kind_of_surface;

    public CleaningProducts(String brend, String destination, String volume, int price, String availability, String place_of_application, String kind_of_surface) {
        super(brend, destination, volume, price, availability);
        this.place_of_application = place_of_application;
        this.kind_of_surface = kind_of_surface;
    }

    @Override
    public String toString() {
        return "CleaningProducts{" + super.toString()+
                "place_of_application='" + place_of_application + '\'' +
                ", kind_of_surface='" + kind_of_surface + '\'' +
                '}';
    }
}
