package com.epam;

public class HouseholdChemicals {
    private String brend;
    private String destination;
    private String volume;
    private int price;
    private String availability;

    public HouseholdChemicals(String brend, String destination, String volume, int price, String availability) {
        this.brend = brend;
        this.destination = destination;
        this.volume = volume;
        this.price = price;
        this.availability = availability;
    }

    public String getBrend() {
        return brend;
    }

    public void setBrend(String brend) {
        this.brend = brend;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    @Override
    public String toString() {
        return "HouseholdChemicals{" +
                "brend='" + brend + '\'' +
                ", destination='" + destination + '\'' +
                ", volume='" + volume + '\'' +
                ", price=" + price +
                ", availability='" + availability + '\'' +
                '}';
    }
}
